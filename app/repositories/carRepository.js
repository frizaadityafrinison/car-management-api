const { Car } = require('../models');

module.exports = {
    create(bodyrequest) {
        return Car.create(bodyrequest);
    },
    read() {
        return Car.findAll();
    },
    update(id, bodyRequest) {
        return Car.update(id, bodyRequest);
    },
    delete(bodyRequest, id) {
        return Car.findOne({
            where: {
                id
            },
        }).then((deletedCar) => {
            deletedCar.update({
                deletedBy: bodyRequest.user.role
            })
        }).then(() => {
            Car.destroy({
                where: {
                    id
                }
            })
        })
    }
}