const { User } = require('../models');

module.exports = {
    create(bodyRequest) {
        return User.create(bodyRequest);
    },
    login(bodyRequest) {
        return User.findOne(bodyRequest);
    },
    findUser(token) {
        return User.findByPk(token);
    }
}
