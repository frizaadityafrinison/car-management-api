'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Car.init({
    name: DataTypes.TEXT,
    price: DataTypes.TEXT,
    image: DataTypes.TEXT,
    createdBy: DataTypes.TEXT,
    deletedBy: DataTypes.TEXT,
    deletedAt: DataTypes.TEXT,
    updatedBy: DataTypes.TEXT
  }, {
    sequelize,
    paranoid: true,
    modelName: 'Car',
  });
  return Car;
};