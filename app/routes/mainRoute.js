const express = require('express');
const router = express.Router();
const YAML = require('yamljs');
const swaggerUI = require('swagger-ui-express');

const swaggerDocument = YAML.load('./documentation.yaml');
const carController = require('../controllers/carController');
const userController = require('../controllers/userController');
const authService = require('../services/authService');

// OPEN API DOCUMENTATION
router.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

// ADD ADMIN ROUTE
router.post('/registeradmin', authService.authorize, authService.authorizeAddAdmin, userController.registerAdmin);

// CRUD ROUTE
router.post('/cars', authService.authorize, authService.authorizePrivilege, carController.createCar);
router.get('/cars', authService.authorize, authService.authorizePrivilege, carController.readCar);
router.patch('/cars/:id', authService.authorize, authService.authorizePrivilege, carController.updateCar);
router.delete('/cars/:id', authService.authorize, authService.authorizePrivilege, carController.deleteCar);

// SEE THE LIST OF CAR
router.get('/listcars', authService.authorize, authService.authorizeUser, carController.readCar);

// AUTHENTICATION & AUTHORIZATION ROUTE
router.post('/register', userController.register);
router.post('/login', userController.signin);

// GET CURRENT USER
router.get('/currentuser', authService.authorize, userController.findCurrentUser);

module.exports = router;