const userService = require('../services/userService');

module.exports = {
    register: async (req, res) => {
        try {
            const data = await userService.createUser(req);
            res.status(200).json({
                status: "OK",
                success: true,
                error: false,
                data: data,
            });
        } catch (error) {
            res.status(400).json({
                status: "ERROR",
                success: false,
                error: true,
                data: null,
            });
        }
    },
    registerAdmin: async (req, res) => {
        try {
            const data = await userService.createUserAdmin(req);
            res.status(200).json({
                success: true,
                error: false,
                data: data,
                status: "OK"
            });
        } catch (error) {
            console.log(error);
            res.status(400).json({
                success: false,
                error: true,
                data: null,
                status: "Error"
            });
        }
    },
    signin: async (req, res) => {
        try {
            const result = await userService.loginUser(req);
            const { data, message, token} = result;

            if(!data) {
                return res.status(401).json({
                    success: false,
                    error: true,
                    data,
                    message: "Error"
                });
            }
            res.status(200).json({
                success: true,
                error: false,
                data,
                token,
                message: "Successfully Login"
            });
        } catch (error) {
            res.status(400).json({
                success: false,
                error: true,
                data,
                message: error
            })
        }
    },
    findCurrentUser: async (req, res) => {
        res.status(201).json(req.user);
    },
}