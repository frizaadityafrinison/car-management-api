const carService = require('../services/carService');

module.exports = {
    createCar: async (req, res) => {
        try {
            const data = await carService.createData(req);
            res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: "SUCCESSFULLY CREATED A CAR"
            });
        } catch (error) {
            console.log(error);
            res.status(200).json({
                success: false,
                error: true,
                data: null,
                message: "FAILED CREATED A CAR",
            });
        }
    },
    readCar: async (req, res) => {
        try {
            const data = await carService.readData();
            res.status(200).json({
                success: true,
                error: false,
                data,
                message: "SUCCESSFULLY LISTED ALL CAR"
            })
        } catch (error) {
            console.log(error);
            res.status(200).json({
                success: false,
                error: true,
                data: null,
                message: "FAIL LISTED ALL CAR",
            });
        }
    },
    updateCar: async (req, res) => {
        try {
            const data = await carService.updateData(req.params.id, req);
            res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: "SUCCESSFULLY UPDATED A CAR"
            });
        } catch (error) {
            console.log(error);
            res.status(200).json({
                success: false,
                error: true,
                data: null,
                message: "FAILED UPDATED A CAR",
            });
        }
    },
    deleteCar: async (req, res) => {
        try {
            await carService.deleteData(req, req.params.id);
            res.status(200).json({
                success: true,
                error: false,
                message: "SUCCESSFULLY DELETED A CAR"
            });
        } catch (error) {
            console.log(error);
            res.status(200).json({
                success: false,
                error: true,
                data: null,
                message: "FAILED DELETED A CAR",
            });
        }
    }
}