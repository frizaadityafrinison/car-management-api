const userRepository = require('../repositories/userRepository');
const authService = require('../services/authService');

module.exports = {
    createUser: async (bodyRequest) => {
        const email = bodyRequest.body.email.toLowerCase();
        const password = await authService.encryptPassword(bodyRequest.body.password);
        const role = "member";

        const user = await userRepository.login( { where: { email: email } } );
        if (user) {
            return {
                data: null,
                message: 'Email Already Used'
            }
        } else {
            return await userRepository.create({
                email: email,
                password: password,
                role: role,
            });
        }
        
    },
    createUserAdmin: async (bodyRequest) => {
        const email = bodyRequest.body.email;
        const password = await authService.encryptPassword(bodyRequest.body.password);
        const role = "admin";

        const user = await userRepository.login( { where: { email: email } } );
        if (user) {
            return {
                data: null,
                message: 'Email Already Used'
            }
        } else {
            return await userRepository.create({
                email: email,
                password: password,
                role: role,
            });
        }
    },
    loginUser: async (bodyRequest) => {
        const email = bodyRequest.body.email;
        const password = bodyRequest.body.password;

        const data = await userRepository.login( { where: { email } } );
        if (!data) {
            return {
                data: null,
                message: 'User Not Found'
            }
        }

        const isPasswordMatch = await authService.checkPassword(password, data.password);
        if (!isPasswordMatch) {
            return {
                data: null,
                message: 'Password Is Incorrect'
            }
        }

        const token = authService.createToken({
            id: data.id,
            email: data.email
        });

        data.token = token;

        return {
            data: data,
            token,
            message: 'User Found'
        }
    },
}