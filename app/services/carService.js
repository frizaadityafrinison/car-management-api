const carRepository = require('../repositories/carRepository');

module.exports = {
    createData: async (bodyRequest) => {
        return carRepository.create({
            name: bodyRequest.body.name,
            price: bodyRequest.body.price,
            image: bodyRequest.body.image,
            createdBy: bodyRequest.user.role,
        });
    },
    readData: async () => {
        return await carRepository.read();
    },
    updateData: async (id, bodyRequest) => {
        return await carRepository.update({
            name: bodyRequest.body.name,
            price: bodyRequest.body.price,
            image: bodyRequest.body.image,
            updatedBy: bodyRequest.user.role
        }, { where: { id: id } });
    },
    deleteData: async (bodyRequest, id) => {
        return await carRepository.delete(bodyRequest, id);
    }
}