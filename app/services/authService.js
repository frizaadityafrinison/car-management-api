const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const userRepository = require('../repositories/userRepository');

module.exports = {
    encryptPassword: (password) => {
        return new Promise((resolve, reject) => {
            bcrypt.hash(password, 10, (err, encryptedPassword) => {
                if (!!err) {
                    return reject(err);
                }

                resolve(encryptedPassword);
            });
        });
    },
    checkPassword: (password, encryptedPassword) => {
        return new Promise((resolve, reject) => {
            bcrypt.compare(password, encryptedPassword, (error, isPasswordMatch) => {
                if(!!error) {
                    return reject(error);
                }

                resolve(isPasswordMatch);
            });
        })
    },
    createToken: (payload) => {
        return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Secret");
    },
    authorize: async (req, res, next) => {
        try {
            const bearerToken = req.headers.authorization;
            const token = bearerToken.split("Bearer ")[1];
            const verifyTokenResult = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "Secret");
            req.user = await userRepository.findUser(verifyTokenResult.id);
            next();
        } catch (error) {
            console.log(error);
            res.status(401).json({
                message: "Unauthorized",
                status: "Error"
            });
        }
    },
    authorizePrivilege: async (req, res, next) => {
        try {
            if (req.user.role.toLowerCase() !== "superadmin" && req.user.role.toLowerCase() !== "admin") {
                res.status(401).json({
                    message: "You Have No Privilege",
                    yourRole: req.user.role,
                    status: "Error"
                });
            } else {
                next();
            }
        } catch (error) {
            console.log(error);
            res.status(401).send({
                message: "Unauthorized",
            });          
        }
    },
    authorizeAddAdmin: async (req, res, next) => {
        try {
            if (req.user.role.toLowerCase() !== "superadmin") {
                res.status(401).json({
                    message: "You Have No Privilege",
                    yourRole: req.user.role,
                    status: "Error"
                });
            } else {
                next();
            }
        } catch (error) {
            console.log(error);
            res.status(401).send({
                message: "Unauthorized",
            });          
        }
    },
    authorizeUser: async (req, res, next) => {
        try {
            if (!req.user.role) {
                res.status(401).json({
                    message: "You Have No Privilege",
                    yourRole: req.user.role,
                    status: "Error"
                });
            } else {
                next();
            }
        } catch (error) {
            console.log(error);
            res.status(401).send({
                message: "Unauthorized",
            });          
        }
    }
}