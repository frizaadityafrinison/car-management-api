# Car Management API (Chapter 6 - Main Challenge)

A simple RESTful API server made with love® and determination™

## Description

Credential For Superadmin
```bash
email: superadmin@email.com
password: superadmin123
```

Open API Document
```bash
http://localhost:8080/api-docs/
```

## Tech Stack
  * Node.js 16.14 (Runtime Environment)
  * Express 4.17.3 (HTTP Server)
  * Sequelize 6.19.0 (ORM)
  * PostgreSQL 14.2 (DBMS)
  * Nodemon 2.0.16 (Server Runner)
  * Bcryptjs 2.4.3 (Encryption)
  * JSON Web Tokens 8.5.1 (Token For Request)
  * Swagger UI Express 4.3.0 (Open API Documentation) 
  * YAMLJS 0.3.0 (YAML File Management)

## Prerequisite

These are things that you need to install on your machine before proceed to installation
* [Node.js](https://nodejs.org/)
* [NPM (Package Manager)](https://www.npmjs.com/)
* [PostgreSQL](https://www.postgresql.org/) 
* [Postman](https://www.postman.com/)
* [Nodemon](https://www.npmjs.com/package/nodemon)


## Installation

Clone this repository

```bash
cd desired_directory/
git clone https://gitlab.com/frizaadityafrinison/car-management-api
cd car-management-api
```

Download all the package and it's dependencies
```bash
npm install 
```

Install the Sequelize
```bash
npm install sequelize-cli -g
```

Config the Database
```bash
cd app/
cat config/config.json
```

Create The Database
```bash
sequelize db:create
```

Create User Model
```bash
sequelize model:generate --name User --attributes email:text,password:text,role:text
```

Run The Migration
```bash
sequelize db:migrate
``` 

Seed The Data
```bash
sequelize db:seed:all
``` 

## Run The Server

Run the following command to start the server

```bash
  npm run start
```
